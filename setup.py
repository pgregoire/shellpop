from setuptools import setup


setup(name='shellpop',
      version='0.39',
      description='Bind and Reverse shell code generator to aid Penetration Tester in their work.',
      url='https://gitlab.com/pgregoire/shellpop.git',
      author='zc00l, lowfuel, touhidshaikh, fob',
      author_email='git@pgregoire.xyz',
      license='MIT',
      packages=['shellpop'],
      package_dir={"shellpop": "src"},
      scripts=["bin/shellpop"],
      zip_safe=False
)
