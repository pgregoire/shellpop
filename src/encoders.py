from urllib.parse import quote
from base64 import b64encode


def b64e(data):
    return b64encode(data.encode('utf-8')).decode('utf-8')


def hexlify(data):
    return ''.join(['{:02x}'.format(ord(b)) for b in data])


def to_urlencode(data):
    """
    URL-encode a byte stream, plus some other characters that
    might be ignored by quote() from urllib.
    @zc00l
    """
    additional = [".", "/"]
    data = quote(data)
    for each in additional:
        data = data.replace(each, "%" + hexlify(each))
    return data


def to_unicode(data):
    """
    Get a string and make it Unicode
    @zc00l
    """
    out = ""
    for char in data:
        out += char + "\x00"
    return out


def powershell_base64(data, unicode_encoding=True):
    """
    Encode something compatible for Powershell base64-encoding
    Default behaviour is to encode Unicode before Base64.
    @zc00l
    """
    data = to_unicode(data) if unicode_encoding else data
    return b64e(data).replace("\n", "")


def xor(data, key):
    """
    XOR a byte-stream with a single key value (int)
    """
    if type(key) is not int:
        return None

    return ''.join([chr(ord(c) ^ key) for c in data])
